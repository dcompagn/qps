/* Versione _ripulita_ da debug, trace, like e con release non ottimizzato */
/* This version is implemented considering A/B sets bipartitioned offline */
/* and, thus, keeping two ready queues for each processor. */

#include <asm/uaccess.h>
#include <linux/percpu.h>
#include <linux/sched.h>
#include <linux/list.h>
#include <linux/spinlock.h>
#include <linux/module.h>
#include <linux/slab.h>

#include <litmus/litmus.h>
#include <litmus/jobs.h>
#include <litmus/preempt.h>
#include <litmus/budget.h>
#include <litmus/sched_plugin.h>
#include <litmus/edf_common.h>
#include <litmus/sched_trace.h>
#include <litmus/trace.h>

#include <litmus/sched_qps.h>
/* FIXED: At PAR_TO_UNI we check for set switching */

/* NOTE: We have to prevent the master activation as long as it has a small budget slice. */

/* Monotonic time in schedule function (no ref to release time in schedule)*/
/* No update after budget exhaustion */

/* FIX: put masters in the queues */

/* TODO: complete the release, fix the exit conditions, fix the schedule function */

/* HINT: to identify the interval we can check the current time and the qps_domain's deadline */

/* TODO: Global release (following the lut) and dynamically selected execution set*/
/* FIX: check if switch happens just before master execution */
/* FIX: exit of P1 release of P0 enables P2 which is in an exit status */
/* FIX: (ZL) master execute over the release theoretically missing its deadline */
/* zl-flag on task*/
/* FIX: The problem seems to rise when remote proc executes master which processor has got QPS mode off */

/* Main release difficulties to overcome: */
/* - multiple release (see the armed timer) */
/* - schedule and release concurring in the same proc */
/* - release may happened while the master is executing? (e.g. in a different processor?)*/

/* FIX: The lock protocol consist of two-level lock. */
/* One is associated to the cpu while the other is */
/* associated to the execution set. */
/* (global lock/preempt disable/two-lock) */

/* NOTE: The master task always takes charge of scheduling its client */

DEFINE_PER_CPU(qps_domain_t, qps_domains);

raw_spinlock_t 	glock;
lt_t 			sync_release_time;
qps_domain_t	*dlt[MAX_CPU];

#define QPS_SMALL_BUDGET_PREVENTION
#define QPS_BRANCH_OPTIMIZATION
#define QPS_CLUSTERED_LOCK
//#define QPS_ALL_BUG
//#define QPS_ALL_TRACE
//#define QPS_ALL_TRACE
#define QPS_AVOID_IPI

#ifdef QPS_ALL_BUG
	#define DEBUG_MAX_LATENCY 1000000LL
#endif
#ifdef QPS_SMALL_BUDGET_PREVENTION
	#define QPS_MIN_ACTIVATION_BUDGET 90000LL
#endif
#ifdef QPS_BRANCH_OPTIMIZATION
	#define qps_likely(cond)			likely(cond)
	#define qps_unlikely(cond)			unlikely(cond)
#else
	#define qps_likely(cond)			cond
	#define qps_unlikely(cond)			cond
#endif

//USEFUL DEFINES
#define local_qps_domain				(&__get_cpu_var(qps_domains))
#define remote_qps_domain(cpu)			(&per_cpu(qps_domains, cpu))
#define task_qps_domain(task)			remote_qps_domain(get_partition(task))

#define master_tsk(task)				(tsk_rt(task)->task_params)
#define is_master(task)					(task && master_tsk(task).master)
#define is_master_executing(task) 		(is_master(task) && (master_tsk(task).executing_cpu != NO_CPU))
#define scheduled(task, set)			(task == set->scheduled)
/* ZL condition hold only if we check the task at its deadline/release */
#define is_zero_laxity(task, set, time)	(is_master(task) && scheduled(task, set) && lt_after_eq(time, tsk_rt(task)->job_params.deadline))
#define client_qps_domain(task)			(remote_qps_domain(master_tsk(task).reference_cpu))
#define client_exec_set(task)			(client_qps_domain(task)->master_set)
#define get_exec_set(qps_domain, task)	(tsk_rt(task)->task_params.set ? &qps_domain->b_set : &qps_domain->a_set)
#define get_safe_deadline(task)			(task ? get_deadline(task) : LLONG_MAX)
#define system_active					(lt_after_eq(litmus_clock(), sync_release_time))

static enum hrtimer_restart qps_on_timer_fired(struct hrtimer *timer)
{
	qps_domain_t* qps_domain = container_of(timer, qps_domain_t, timer);
	lt_t current_time;
	unsigned long flags;

	current_time = litmus_clock();

#ifdef QPS_ALL_BUG
	BUG_ON(smp_processor_id() != qps_domain->cpu);
	BUG_ON(lt_after(current_time, ktime_to_ns(timer->_softexpires) + DEBUG_MAX_LATENCY));
#endif

/*#ifdef QPS_CLUSTERED_LOCK
	raw_spin_lock_irqsave(&dlt[qps_domain->cpu]->slock, flags);
#else
	raw_spin_lock_irqsave(&glock, flags);
#endif*/
	
	local_irq_save(flags);
#ifdef QPS_ALL_TRACE
	TRACE("enforcement timer armed for %llu fired at %llu\n", 
		ktime_to_ns(timer->_softexpires) - sync_release_time, current_time - sync_release_time);
#endif
	if (qps_likely(lt_after_eq(current_time, ktime_to_ns(timer->_softexpires))))
		qps_domain->armed = 0;
	/* activate scheduler */
	/*litmus_reschedule(qps_domain->cpu);*/
	litmus_reschedule_local();
	local_irq_restore(flags);

/*#ifdef QPS_CLUSTERED_LOCK
	raw_spin_unlock_irqrestore(&dlt[qps_domain->cpu]->slock, flags);
#else
	raw_spin_unlock_irqrestore(&glock, flags);
#endif*/
	return  HRTIMER_NORESTART;
}

static enum hrtimer_restart qps_on_dl_timer_fired(struct hrtimer *timer)
{
	qps_domain_t* qps_domain = container_of(timer, qps_domain_t, timer);
	unsigned long flags;

#ifdef QPS_CLUSTERED_LOCK
	raw_spin_lock_irqsave(&dlt[qps_domain->cpu]->slock, flags);
#else
	raw_spin_lock_irqsave(&glock, flags);
#endif
	//local_irq_save(flags);
#ifdef QPS_ALL_TRACE
	TRACE("dl_timer fired.\n");
#endif
	qps_domain->dl_armed = 0;
	qps_domain->exec_mode = S_QPS_OFF;
	/* activate scheduler */
	litmus_reschedule(qps_domain->cpu);
	//local_irq_restore(flags);
#ifdef QPS_CLUSTERED_LOCK
	raw_spin_unlock_irqrestore(&dlt[qps_domain->cpu]->slock, flags);
#else
	raw_spin_unlock_irqrestore(&glock, flags);
#endif

	return  HRTIMER_NORESTART;
}

static void exec_set_init(exec_set_t *exec_set,
							check_resched_needed_t check, 
							release_jobs_t release, 
							int cpu) 
{
	exec_set->cpu = cpu;

	edf_domain_init(&exec_set->domain, check, release);
	exec_set->scheduled	= NULL;
	
	exec_set->exec_time = 0LL;
	exec_set->budget = 0LL;
	exec_set->start_exec = 0LL;
	exec_set->stop_exec = 0LL;

	exec_set->u_num	= 0LL;
	exec_set->u_den	= 1LL;
}

static void qps_domain_init(qps_domain_t *qps_domain,
							check_resched_needed_t check, 
							release_jobs_t release, 
							int cpu) 
{
	qps_domain->cpu = cpu;
	qps_domain->deadline = LLONG_MAX; /* At start we check if the release time if after than e_dl */
	qps_domain->release = LLONG_MAX;
	qps_domain->something_released = 0;

	qps_domain->master = NULL;

	exec_set_init(&qps_domain->a_set, check, release, cpu);
	exec_set_init(&qps_domain->b_set, check, release, cpu);
	
	qps_domain->master_set = &qps_domain->b_set;
	qps_domain->slave_set = &qps_domain->a_set;
	qps_domain->exec_mode = S_QPS_OFF;

	hrtimer_init(&qps_domain->timer, CLOCK_MONOTONIC, HRTIMER_MODE_ABS);
	qps_domain->timer.function = qps_on_timer_fired;
	qps_domain->armed = 0;

	hrtimer_init(&qps_domain->dl_timer, CLOCK_MONOTONIC, HRTIMER_MODE_ABS);
	qps_domain->dl_timer.function = qps_on_dl_timer_fired;
	qps_domain->dl_armed = 0;
	
	qps_domain->exit = 0;

	bheap_init(&qps_domain->fake_heap);
}

/* Put a concrete task into the _QPS_ release heap */
void qps_add_task_to_fake_queue(qps_domain_t *qps_domain, struct task_struct *t)
{
	if(!bheap_node_in_heap(tsk_rt(t)->qps_heap_node)) {
		bheap_insert(qps_release_order, &qps_domain->fake_heap, tsk_rt(t)->qps_heap_node);
#ifdef QPS_ALL_TRACE		
		TRACE("Added into fake release queue\n");
#endif
	}
}

/* Delete all tasks from the _QPS_ release heap */
/* which release time is before or equal to that passed */
void qps_update_fake_queue(qps_domain_t *qps_domain, lt_t release_time)
{
	struct bheap_node* hn = bheap_peek(qps_release_order, &qps_domain->fake_heap);
	struct task_struct *t;

	while(hn) {
		t = bheap2task(hn);
	 	if (lt_after_eq(release_time, get_release(t))) 
	 	{
	 		bheap_delete(qps_release_order, &qps_domain->fake_heap, tsk_rt(t)->qps_heap_node);
	 		hn = bheap_peek(qps_release_order, &qps_domain->fake_heap);
	 	} else {
	 		hn = NULL;
	 	}
	}
#ifdef QPS_ALL_TRACE
	TRACE("QPS release queue update until %llu\n", release_time - sync_release_time);
#endif
}

/* assumes called with IRQs off */
void qps_arm_enforcement_timer(qps_domain_t *qps_domain, lt_t time)
{
	if (lt_after(litmus_clock(), time))
		TRACE("WARNING: arming timer in the past\n");
	if (!qps_domain->armed || (ktime_to_ns(qps_domain->timer._softexpires) != time)) {
		__hrtimer_start_range_ns(&qps_domain->timer,
					 ns_to_ktime(time),
					 0 /* delta */,
					 HRTIMER_MODE_ABS_PINNED,
					 0 /* no wakeup */);
		qps_domain->armed = 1;
	}
#ifdef QPS_ALL_TRACE
	TRACE("Timer armed to %llu\n", time - sync_release_time);
#endif
}

/* assumes called with IRQs off */
static void qps_cancel_enforcement_timer(qps_domain_t *qps_domain)
{
	int ret;

	if (qps_domain->armed) {
		ret = hrtimer_try_to_cancel(&qps_domain->timer);
#ifdef QPS_ALL_BUG		
		/* Should never be inactive. */		
		BUG_ON(ret == 0);
		/* Should never be running concurrently. */
		BUG_ON(ret == -1);
#endif
		qps_domain->armed = 0;
#ifdef QPS_ALL_TRACE
		TRACE("Timer cancelled\n");
#endif
	}
}

static void switch_exec_set(qps_domain_t *dom) {
	exec_set_t *tmp;
	BUG_ON(dom->master_set == dom->slave_set);
	tmp = dom->master_set;
	dom->master_set = dom->slave_set;
	dom->slave_set = tmp;
#ifdef QPS_ALL_TRACE
	TRACE("exec_set switched\n");
#endif
}

/* Update the master task budget. 			*/
/* INVARIANTS: 								*/
/* 		release and deadline are up to date */
/*		release < deadline 					*/
/*		t is a master task 					*/
/*		release(t) < release 				*/
static void qps_replenish_master(struct task_struct *t, lt_t release, lt_t deadline) 
{	
#ifdef QPS_ALL_BUG
	BUG_ON(!is_master(t));
#endif
	/* Invalid for sporadic activation */
	/*BUG_ON(lt_after(tsk_rt(t)->job_params.release, release));*/
	tsk_rt(t)->job_params.release = release;
	tsk_rt(t)->job_params.deadline = deadline;
	tsk_rt(t)->job_params.budget += master_budget_for_interval(t, release, deadline);
#ifdef QPS_ALL_TRACE	
	TRACE("master updated +%llu [%llu,%llu] (%llu, %llu) job=%d\n", 
		master_budget_for_interval(t, release, deadline), release - sync_release_time, deadline - sync_release_time, 
		tsk_rt(t)->job_params.exec_time, tsk_rt(t)->job_params.budget, tsk_rt(t)->job_params.job_no);
#endif
}

/* Update the execution set budget.			*/
/* INVARIANTS: 								*/
/* 		release and deadline are up to date */
/*		release < deadline 					*/
/*		budget < exec_time 					*/
static void qps_replenish_exec_set(exec_set_t *exec_set, lt_t release, lt_t deadline) 
{
	exec_set->budget += exec_set_budget_for_interval(exec_set, release, deadline);
#ifdef QPS_ALL_TRACE
	TRACE("exec_set updated +%llu [%llu,%llu] (%llu, %llu)\n",
		exec_set_budget_for_interval(exec_set, release, deadline), 
		release - sync_release_time, deadline - sync_release_time, exec_set->exec_time, exec_set->budget);
#endif
}

void qps_prepare_for_next_period(struct task_struct *t)
{
	if (is_master(t)) 
	{
		tsk_rt(t)->job_params.release = tsk_rt(t)->job_params.deadline;
		tsk_rt(t)->job_params.job_no++;
		tsk_rt(t)->dont_requeue = 0;
	} else
		prepare_for_next_period(t);
}

static void job_completion(struct task_struct* t, int forced)
{
	if (!is_master(t)) {
		sched_trace_task_completion(t,forced);
#ifdef QPS_ALL_TRACE		
		TRACE_TASK(t, "job_completion().\n");
#endif
	} 
#ifdef QPS_ALL_TRACE
	else
		TRACE("job_completion() master, forced = %d\n", forced ? 1 : 0);
#endif

	tsk_rt(t)->completed = 0;
	qps_prepare_for_next_period(t);
}

static void requeue(struct task_struct* t, exec_set_t *exec_set)
{
	/* where the release queue is located */
	qps_domain_t *local_domain = remote_qps_domain(exec_set->cpu);
	qps_domain_t *remote_domain = dlt[local_domain->cpu];

#ifdef QPS_ALL_BUG	
	BUG_ON(local_domain->cpu != get_partition(t));
	BUG_ON(remote_domain->cpu < local_domain->cpu);
	BUG_ON(!remote_domain);
#endif

	tsk_rt(t)->completed = 0;
	/*TRACE("Requeuing task into (%d, %d) remote %d\n", 
		local_domain->cpu, &local_domain->a_set == exec_set ? 0 : 1, remote_domain->cpu);*/
	/* A budget echausted master may be requeued in master if a schedule */
	/* occurs after the deadline but before the new release. */
	if (is_released(t, litmus_clock()) && 
		(!is_master(t) || master_budget_remaining(t))) {
		__add_ready(&exec_set->domain, t);
	} else {
		qps_add_task_to_fake_queue(local_domain, t);
		if (!is_master(t)) {
#ifdef CONFIG_RELEASE_MASTER
			add_release_on(&remote_domain->a_set.domain, t, remote_domain->cpu);
#else
			add_release(&remote_domain->a_set.domain, t);
#endif
		}
	}
}

/* we assume the lock is being held */
static void preempt(int cpu)
{
	/*preempt_if_preemptable(NULL, cpu);*/
#ifdef QPS_ALL_TRACE	
	TRACE("Preempting cpu %d\n", cpu);
#endif
	litmus_reschedule(cpu);
}

/* Return the earliest release time of a domain */
lt_t qps_next_release_time(qps_domain_t *qps_domain)
{
	struct bheap_node* hn = 
		bheap_peek(qps_release_order, &qps_domain->fake_heap);
	//TRACE("Next release time is %llu\n", hn ? get_release(bheap2task(hn)) : LLONG_MAX);
	return hn ? get_release(bheap2task(hn)) : LLONG_MAX;
}

/* Can we use a lookup table to determine the earliest deadline among hierarchy? */
/* INVARIANTS: */
/* 		both release and fake_queue are up to date */
static lt_t get_earliest_deadline(qps_domain_t *qps_domain)
{
	lt_t a_sched, a_rd, b_sched, b_rd, e_rl, e_dl;
	
	/* At ZL master is updated from the qps_release_jobs */
	a_sched = get_safe_deadline(qps_domain->a_set.scheduled);
	a_rd = get_safe_deadline(__peek_ready(&qps_domain->a_set.domain));

	b_sched = get_safe_deadline(qps_domain->b_set.scheduled);
	b_rd = get_safe_deadline(__peek_ready(&qps_domain->b_set.domain));

	e_rl = qps_next_release_time(qps_domain);
	e_dl = qps_min(qps_min(qps_min(a_sched, a_rd), qps_min(b_sched, b_rd)), e_rl);
#ifdef QPS_ALL_TRACE
	TRACE("earliest deadline %llu (a(%llu, %llu), b(%llu, %llu) rl=%llu)\n", 
		e_dl - sync_release_time, a_sched - sync_release_time, 
		a_rd - sync_release_time, b_sched - sync_release_time, 
		b_rd - sync_release_time, e_rl - sync_release_time);
#endif
	return e_dl;
}

static int __switch_exec_set(qps_domain_t * qps_domain)
{

	lt_t e_dl_master, e_dl_slave;
	struct task_struct *tmp_tsk;
	
	tmp_tsk = edf_preemption_needed(&qps_domain->slave_set->domain, 
		qps_domain->slave_set->scheduled) ? 
		__peek_ready(&qps_domain->slave_set->domain) :
		qps_domain->slave_set->scheduled;

	e_dl_slave = get_safe_deadline(tmp_tsk);

	tmp_tsk = edf_preemption_needed(&qps_domain->master_set->domain, 
		qps_domain->master_set->scheduled) ? 
		__peek_ready(&qps_domain->master_set->domain) :
		qps_domain->master_set->scheduled;

	e_dl_master = get_safe_deadline(tmp_tsk);

	return lt_after(e_dl_slave, e_dl_master);
}

static void qps_release_jobs(rt_domain_t* rt, struct bheap* tasks)
{
	lt_t 				release_time;
	lt_t 				deadlines[MAX_CPU];
	unsigned int 		cpu, master_cpu;
	exec_set_t 			*exec_set = container_of(rt, exec_set_t, domain);
	exec_set_t 			*master_exec_set = NULL;
	qps_domain_t 		*qps_domain = remote_qps_domain(exec_set->cpu);
	qps_domain_t 		*released[MAX_CPU];
	struct task_struct 	*tmp_tsk;
	unsigned long		flags;

	/* CANBE FALSE */
	/*BUG_ON(local_qps_domain != qps_domain);*/

#ifdef QPS_CLUSTERED_LOCK
	raw_spin_lock_irqsave(&dlt[qps_domain->cpu]->slock, flags);
#else
	raw_spin_lock_irqsave(&glock, flags);
#endif
	
	release_time = get_release(bheap2task(bheap_peek(edf_ready_order, tasks)));
	BUG_ON(lt_after(release_time, litmus_clock()));
#ifdef QPS_ALL_TRACE
	TRACE("****** RELEASE for (%d,%d) locking %d at %llu as (rl=%llu, dl=%llu) \n", 
		exec_set->cpu, &qps_domain->a_set == exec_set ? 0 : 1, dlt[qps_domain->cpu]->cpu,
		litmus_clock() - sync_release_time, release_time - sync_release_time, 
		qps_domain->deadline == 0LL ? 0LL : qps_domain->deadline - sync_release_time);
#endif	
	/* Initialize vectors */
	for (cpu = 0; cpu < num_online_cpus(); cpu++) {
		released[cpu] = NULL;
		deadlines[cpu] = LLONG_MAX;
	}

	/* Put each task in its release queue */
	while(!bheap_empty(tasks))
	{
		tmp_tsk = bheap2task(bheap_take(edf_ready_order, tasks));
		cpu = get_partition(tmp_tsk);
		released[cpu] = remote_qps_domain(cpu);
		requeue(tmp_tsk, get_exec_set(released[cpu], tmp_tsk)); // <-- task will be added to the ready queue
#ifdef QPS_ALL_TRACE
		TRACE("requeued in (%d,%d)\n", cpu, tsk_rt(tmp_tsk)->task_params.set);
#endif
	}
	if (qps_unlikely(!system_active)) {
		for (cpu = 0; cpu < num_online_cpus(); cpu++) 
			if (released[cpu])
				preempt(cpu);
#ifdef QPS_ALL_TRACE				
		TRACE("****** END RELEASE ******\n");
#endif
#ifdef QPS_CLUSTERED_LOCK
		raw_spin_unlock_irqrestore(&dlt[qps_domain->cpu]->slock, flags);
#else
		raw_spin_unlock_irqrestore(&glock, flags);
#endif
		/* IMPORTANT! */
		return;
	}

	/* Update the scheduling intervals */
	/* INVARIANTS: queues are updated */
	/* NOTE: qps_domain(s) are visited one time only */
	for (cpu = 0; cpu < num_online_cpus(); cpu++) {	
		if (qps_likely(released[cpu])) {
			if (likely(!released[cpu]->exit)) {
#ifdef QPS_ALL_TRACE
				TRACE("Updating cpu %d\n", cpu);
#endif
#ifdef QPS_ALL_BUG
				BUG_ON(lt_after(LLONG_MAX, released[cpu]->deadline) && 
					lt_after(released[cpu]->deadline, release_time));
#endif
				/* Remove all tasks in the queue whose release */
				/* is before or equal to the actual time. Note that */
				/* masters are handled in the same way (queued) */
				qps_update_fake_queue(released[cpu], release_time);
				released[cpu]->release = release_time;
				/* Since masters are in the queues we can avoid to check the deadlines array (ONLY at the first release it is needed) */
#ifdef QPS_ALL_TRACE
				TRACE("deadlines[%d]=%llu\n", cpu, deadlines[cpu] - sync_release_time);
#endif
				released[cpu]->deadline = qps_min(deadlines[cpu], get_earliest_deadline(released[cpu]));
				/* Not really needed */
				deadlines[cpu] = released[cpu]->deadline;
#ifdef QPS_ALL_TRACE
				TRACE("time interval [%llu, %llu]\n", released[cpu]->release - sync_release_time, released[cpu]->deadline - sync_release_time);
#endif
				/* INVARIANT: the new time interval is updated (release and deadline) */
				/*BUG_ON(lt_after_eq(released[cpu]->release, released[cpu]->deadline));*/
				/* Safe exit */
				if (qps_likely(lt_after(released[cpu]->deadline, released[cpu]->release))) {
					BUG_ON(lt_after(litmus_clock(), released[cpu]->deadline));
#ifdef QPS_ALL_BUG						
					BUG_ON(released[cpu]->slave_set == released[cpu]->master_set);
#endif
					/* We update the consumed budget as long as */
					/* an overrunning exec_set might still consume budget if replenished */
					if (released[cpu]->exec_mode == S_UNI || released[cpu]->exec_mode == S_UNI_TO_PAR)
						exec_set_update_exec_time(released[cpu]->slave_set, release_time);
					if (released[cpu]->exec_mode == S_UNI || released[cpu]->exec_mode == S_PAR_TO_UNI)
						exec_set_start_exec(released[cpu]->slave_set, release_time);

					if(lt_after(exec_set_budget_remaining(released[cpu]->slave_set), 0LL)) // zero conceptually
						exec_set_empty_budget(released[cpu]->slave_set);
					if (lt_after(exec_set_budget_remaining(released[cpu]->master_set), 0LL))
						exec_set_empty_budget(released[cpu]->master_set);

					/* The release overhead is take into account when the slave_set 
					is running among the time interval */
#ifdef QPS_SMALL_BUDGET_PREVENTION					
					if (likely(lt_after(exec_set_budget_for_interval(released[cpu]->slave_set, 
						released[cpu]->release, released[cpu]->deadline), QPS_MIN_ACTIVATION_BUDGET)))
#endif
						qps_replenish_exec_set(released[cpu]->slave_set, released[cpu]->release, released[cpu]->deadline);
#ifdef QPS_ALL_TRACE			
					/*else TRACE("WARNING: Too little slave set\n");*/
#endif
#ifdef QPS_SMALL_BUDGET_PREVENTION					
					if (likely(lt_after(exec_set_budget_for_interval(released[cpu]->master_set, 
						released[cpu]->release, released[cpu]->deadline), QPS_MIN_ACTIVATION_BUDGET)))
#endif
						qps_replenish_exec_set(released[cpu]->master_set, released[cpu]->release, released[cpu]->deadline);
#ifdef QPS_ALL_TRACE			
					/*else TRACE("WARNING: Too little master set\n");*/
#endif
					if (released[cpu]->master) {
						master_cpu = get_partition(released[cpu]->master);
						/* INVARIANT: parent's cpu is always greater than current updating cpu */
#ifdef QPS_ALL_BUG
						BUG_ON(cpu > master_cpu);
#endif
						/* important: we enable the parent qps_domain update */
						released[master_cpu] = remote_qps_domain(master_cpu);
						master_exec_set = get_exec_set(released[master_cpu], released[cpu]->master);
						/* INVARIANT: master's release time is before actual time*/
						/* 			  master has to be completed its execution (budget) */
						/* Check if it is already requeued or (scheduled) at ZL */
						/* IMPORTANT: If master executes at ZL then it must be scheduled */
						/* 			  then is not queued, but it compromise the computation of */
						/*			  the earliest deadline. So we must force the completion event */
						/*			  for such a task, and remove it from the scheduled pointer */
						
						/* We prevent the budget release if its budget is too small */

#ifdef QPS_SMALL_BUDGET_PREVENTION
						if (qps_likely(lt_after(master_budget_for_interval(released[cpu]->master,
							released[cpu]->release, released[cpu]->deadline), QPS_MIN_ACTIVATION_BUDGET)))
#endif				
						{
							if (qps_likely(!scheduled(released[cpu]->master, master_exec_set))) {
								qps_replenish_master(released[cpu]->master, 
									released[cpu]->release, released[cpu]->deadline);
								BUG_ON(!is_released(released[cpu]->master, release_time));								
								if (qps_likely(!bheap_node_in_heap(tsk_rt(released[cpu]->master)->heap_node)))
									requeue(released[cpu]->master, master_exec_set);
#ifdef QPS_ALL_TRACE			
								else TRACE("WARNING: master already in ready, miss?\n");
#endif						
							} else if (qps_likely(is_zero_laxity(released[cpu]->master, master_exec_set, release_time))) {
								/* We update the consumed budget as long as */
								/* an overrunning exec_set might still consume budget if replenished */
								if (released[cpu]->exec_mode == S_PAR) {
									master_update_exec_time(released[cpu]->master, release_time);
									master_start_exec(released[cpu]->master, release_time);
								}
								if(lt_after(master_budget_remaining(released[cpu]->master), 0LL)) // zero conceptually
									master_empty_budget(released[cpu]->master);
								job_completion(released[cpu]->master, 1);
								/* The release latency overhead is taken into account */
								qps_replenish_master(released[cpu]->master, 
									released[cpu]->release, released[cpu]->deadline);
#ifdef QPS_ALL_BUG
								BUG_ON(!is_released(released[cpu]->master, release_time));
#endif
							} else {
								/* Deadline missed */
								TRACE("CRITICAL FAULT\n");
#ifdef QPS_ALL_BUG
								BUG_ON(1);
#endif
							}
						}
#ifdef QPS_ALL_TRACE			
						/*else TRACE("WARNING: Too little master\n");*/
#endif
						deadlines[master_cpu] = qps_min(deadlines[master_cpu], deadlines[cpu]);
					}

					if (released[cpu]->exec_mode == S_QPS_OFF) {
						/* We don't want to take into account the budget conumed in the the OFF mode */
						released[cpu]->exec_mode = S_PAR_TO_UNI;
					}

					released[cpu]->something_released = 1;

				} else {
					/* Something goes wrong! We gracefully switch into the exit mode */
					TRACE("CRITICAL FAULT: deadline missed\n");
					released[cpu]->exit = 1;
				}
			}			
			if (qps_unlikely(released[cpu]->exit)) {
				if (released[cpu]->exec_mode == S_UNI || 
					released[cpu]->exec_mode == S_PAR_TO_UNI)
					released[cpu]->exec_mode = S_QPS_OFF;
				if (released[cpu]->master) {
					master_cpu = get_partition(released[cpu]->master);
						/* INVARIANT: parent's cpu is always greater than current updating cpu */
#ifdef QPS_ALL_BUG
						BUG_ON(cpu > master_cpu);
#endif
						/* important: we enable the parent qps_domain update */
					released[master_cpu] = remote_qps_domain(master_cpu);
					released[master_cpu]->exit = 1;
				}
			}
		}
	}

	/* TODO: Non controlla i server che eseguono innestati (costo cpu*cpu) */
	/* Guardare paper su come calcolare schedule in tempo efficente */
#ifdef QPS_AVOID_IPI
	for (cpu = 0; cpu < num_online_cpus(); cpu++) {
		if (released[cpu]) {
			master_exec_set = released[cpu]->slave_set;
			if (edf_preemption_needed(&master_exec_set->domain, master_exec_set->scheduled)) {
				if (is_master(master_exec_set->scheduled)) {
#ifdef QPS_ALL_TRACE					
					TRACE("Avoiding IPI for %d\n", master_tsk(master_exec_set->scheduled).reference_cpu);
#endif
					released[master_tsk(master_exec_set->scheduled).reference_cpu] = NULL;
					//while(master_exec_set->scheduled)
						//released[master_tsk(master_exec_set->scheduled).reference_cpu] = NULL;
						master_exec_set = client_exec_set(master_exec_set->scheduled);
				}
				if (is_master(__next_ready(&master_exec_set->domain))) {
#ifdef QPS_ALL_TRACE
					TRACE("Avoiding IPI for %d\n", master_tsk(__next_ready(&master_exec_set->domain)).reference_cpu);
#endif
					released[master_tsk(__next_ready(&master_exec_set->domain)).reference_cpu] = NULL;						
				}
			}
		}
	}
#endif
	//for (cpu = 0; cpu < num_online_cpus(); cpu++) {
	for (cpu = num_online_cpus()-1; cpu+1 > 0; cpu--) {
		if (released[cpu])
			preempt(cpu);
	}

#ifdef QPS_ALL_TRACE			
	TRACE("****** END RELEASE ******\n");
#endif

#ifdef QPS_CLUSTERED_LOCK
	raw_spin_unlock_irqrestore(&dlt[qps_domain->cpu]->slock, flags);
#else
	raw_spin_unlock_irqrestore(&glock, flags);
#endif

}

static void qps_preempt(struct task_struct * prev_tsk, 
						struct task_struct * next_tsk, 
						int cpu,
						lt_t current_time)
{
	int 			preempt_prev, preempt_next;
	qps_domain_t 	*remote_domain;

	/* prev_tsk or next_tsk may be NULL */
	preempt_prev = 0;
	preempt_next = 0;

	if (is_master(next_tsk)) 
		master_start_exec(next_tsk, current_time);

	if (prev_tsk != next_tsk) 
	{
		preempt_prev = is_master(prev_tsk);
		preempt_next = is_master(next_tsk);
	}
	if (preempt_prev) 
	{
		master_tsk(prev_tsk).executing_cpu = NO_CPU;
#ifdef QPS_ALL_TRACE
		TRACE("switching-out master of %d\n", master_tsk(prev_tsk).reference_cpu);
#endif
		remote_domain = remote_qps_domain(master_tsk(prev_tsk).reference_cpu);
		//BUG_ON(remote_domain->exec_mode != S_PAR && remote_domain->exec_mode != S_UNI_TO_PAR);
		if(remote_domain->exit)
			remote_domain->exec_mode = S_QPS_OFF;
		else 
			remote_domain->exec_mode = S_PAR_TO_UNI;
		preempt(master_tsk(prev_tsk).reference_cpu);
	}
	if (preempt_next) 
	{
		master_tsk(next_tsk).executing_cpu = cpu;
#ifdef QPS_ALL_TRACE
		TRACE("switching-in master of %d\n", master_tsk(next_tsk).reference_cpu);
#endif
		remote_domain = remote_qps_domain(master_tsk(next_tsk).reference_cpu);
		//BUG_ON(remote_domain->exec_mode == S_PAR || remote_domain->exec_mode == S_QPS_OFF);
		remote_domain->exec_mode = S_UNI_TO_PAR;
		preempt(master_tsk(next_tsk).reference_cpu);
	}
}

static struct task_struct* qps_reschedule2(exec_set_t *exec_set, struct task_struct *prev)
{
        /* next == NULL means "schedule background work". */
        struct task_struct *next = NULL;

        /* prev's task state */
        int exists, out_of_time, job_completed, self_suspends, preempt, resched;

        BUG_ON(exec_set->scheduled && exec_set->scheduled != prev);
        BUG_ON(exec_set->scheduled && !is_realtime(prev));

        exists = exec_set->scheduled != NULL;
        if (exists && is_master(prev)) {
        	self_suspends = 0;
        	out_of_time = 0;
        } else {
        	self_suspends = exists && !is_running(prev);
        	out_of_time   = exists && budget_enforced(prev) && budget_exhausted(prev);
        }
        job_completed = exists && is_completed(prev);
        /* preempt is true if task `prev` has lower priority than something on
         * the ready queue. */
        preempt = edf_preemption_needed(&exec_set->domain, prev);

        /* check all conditions that make us reschedule */
        resched = preempt;

        /* if `prev` suspends, it CANNOT be scheduled anymore => reschedule */
        if (self_suspends)
            resched = 1;

        /* also check for (in-)voluntary job completions */
        if (out_of_time || job_completed) {
            job_completion(prev, out_of_time);
            resched = 1;
        }

        if (resched) {
            /* First check if the previous task goes back onto the ready
             * queue, which it does if it did not self_suspend.
             */
            if (exists && !self_suspends)
        		requeue(prev, exec_set);
            next = __take_ready(&exec_set->domain);
        } else
            /* No preemption is required. */
            next = exec_set->scheduled;

        exec_set->scheduled = next;

        return next;
}


//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
////////////////// THE MAIN SCHEDULE FUNCTION ////////////////////
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
static struct task_struct* qps_schedule(struct task_struct * prev)
{
	qps_domain_t 		*local_domain = local_qps_domain;
	exec_set_t 			*bottom_exec_set, *tmp_exec_set;
	struct task_struct 	*tmp_prev_tsk, *tmp_next_tsk, *next_tsk;

	int 				s_o_n, s_i_n, c;
	struct task_struct	*switch_out_stack[MAX_CPU];
	struct task_struct	*switch_in_stack[MAX_CPU];
	
	lt_t				current_time, earliest_event_time;

	int 				switch_between_exec_set, master_exec;
	
#ifdef QPS_CLUSTERED_LOCK
	raw_spin_lock(&dlt[local_domain->cpu]->slock);
#else
	raw_spin_lock(&glock);
#endif
	
	current_time = litmus_clock();
	bottom_exec_set = NULL;
	tmp_next_tsk= NULL; 
	next_tsk = NULL;
	earliest_event_time = LLONG_MAX;
	master_exec = 0;
	switch_between_exec_set = 0;

	if (qps_unlikely(local_domain->exec_mode == S_QPS_OFF)) {
#ifdef QPS_ALL_TRACE		
		TRACE("QPS mode inactive\n");
		if (local_domain->slave_set == &local_domain->a_set)
			TRACE("slave is a\n");
		else
			TRACE("slave is b\n");
#endif
		if (!is_master_executing(local_domain->master))
			switch_between_exec_set = __switch_exec_set(local_domain);
	} else {
		/* Compute the actual time */
		/* NOTE: Keeping the actual release time (virtual) may invalidate 
		the stop_exec > start_exec condition because the time is no longer monotonic */
		/* We align the current reference time to the release to cancel the */
		/* release latency effect on budget accounting */
		/* UPDATE: When exit deadline are not updated and then the current_time is not updated */
		
		if (local_domain->something_released) {
			current_time = local_domain->release;
			local_domain->something_released = 0;
		} else if (lt_after(current_time, local_domain->deadline)) {
#ifdef QPS_ALL_BUG				
				/*BUG_ON(lt_after(current_time - local_domain->deadline, DEBUG_MAX_LATENCY));*/
#endif
				current_time = local_domain->deadline;
		}
#ifdef QPS_ALL_TRACE
		TRACE("****** SCHEDULE at %llu (%llu) locking %d\n", current_time - sync_release_time, litmus_clock() - sync_release_time, dlt[local_domain->cpu]->cpu);
		TRACE("time interval [%llu, %llu]\n", local_domain->release - sync_release_time, local_domain->deadline - sync_release_time);
#endif		
		/* BUG_ON(lt_after(current_time, local_domain->deadline));*/ /*<-- It might be correct since we may schedule after deadline but before the release*/
#ifdef QPS_ALL_BUG
		BUG_ON(lt_after(local_domain->release, local_domain->deadline));
#endif

		if (local_domain->exec_mode == S_UNI)
		{
#ifdef QPS_ALL_TRACE
			TRACE("UNI\n");
#endif
#ifdef QPS_ALL_BUG
			BUG_ON(local_domain->slave_set->start_exec == 0LL);
#endif
			/* The UNI mode takes into account for the budget consumption */
			/* meaning that either A or B set are executing */
#ifdef QPS_ALL_TRACE			
			if (local_domain->slave_set == &local_domain->a_set)
				TRACE("slave is a\n");
			else
				TRACE("slave is b\n");
#endif			
			exec_set_update_exec_time(local_domain->slave_set, current_time);
			switch_between_exec_set = 
				exec_set_budget_exhausted(local_domain->slave_set) && !exec_set_budget_exhausted(local_domain->master_set);
#ifdef QPS_ALL_TRACE
			TRACE("slave  set: (%llu,%llu) start_exec=%llu exec=%llu exhausted=%d overruning=%d\n",
				local_domain->slave_set->exec_time, local_domain->slave_set->budget,
				local_domain->slave_set->start_exec - sync_release_time, current_time - local_domain->slave_set->start_exec, 
				exec_set_budget_exhausted(local_domain->slave_set) ? 1 : 0, 
				(!switch_between_exec_set && exec_set_budget_exhausted(local_domain->slave_set)) ? 1 : 0);
#endif
		} else if (local_domain->exec_mode == S_PAR) 
		{
			/* The PAR mode doesn't take into account for the budget consumption */
			/* meaning that S is executing */
			/* We basically waits for */
#ifdef QPS_ALL_TRACE
			TRACE("PAR\n");
			if (local_domain->slave_set == &local_domain->a_set)
				TRACE("slave is a\n");
			else
				TRACE("slave is b\n");
#endif
		} else if (local_domain->exec_mode == S_UNI_TO_PAR) 
		{
			/* It may happen when master is selected before the first schedule on this processor */
			/*BUG_ON(local_domain->slave_set->start_exec == 0LL);*/
#ifdef QPS_ALL_TRACE
			TRACE("UNI_TO_PAR\n");
			if (local_domain->slave_set == &local_domain->a_set)
				TRACE("slave is a\n");
			else
				TRACE("slave is b\n");
#endif
			/* Update the budget consumed */
			if (qps_likely(lt_after(local_domain->slave_set->start_exec, 0LL))) 
			{
				switch_between_exec_set = exec_set_budget_exhausted(local_domain->slave_set); //We are only reusing the variable
				exec_set_update_exec_time(local_domain->slave_set, current_time);
#ifdef QPS_ALL_TRACE
				TRACE("slave  set: (%llu,%llu) start_exec=%llu exec=%llu exhausted=%d overruning=%d\n", 
					local_domain->slave_set->exec_time, local_domain->slave_set->budget,
					local_domain->slave_set->start_exec - sync_release_time, current_time - local_domain->slave_set->start_exec, 
					exec_set_budget_exhausted(local_domain->slave_set) ? 1 : 0,
					switch_between_exec_set ? 1 : 0);
#endif
				switch_between_exec_set = 0;
			}
			/* switch to the parallel mode */
			local_domain->exec_mode = S_PAR;
		} else if (local_domain->exec_mode == S_PAR_TO_UNI) 
		{
#ifdef QPS_ALL_TRACE			
			TRACE("PAR_TO_UNI\n");
			if (local_domain->slave_set == &local_domain->a_set)
				TRACE("slave is a\n");
			else
				TRACE("slave is b\n");
#endif
			local_domain->exec_mode = S_UNI;
			switch_between_exec_set = 
				exec_set_budget_exhausted(local_domain->slave_set) && !exec_set_budget_exhausted(local_domain->master_set);
		}
	}

	/* Loop over data structure to update the consumed budget */
	s_o_n = 0;
	tmp_exec_set = local_domain->slave_set;
	tmp_prev_tsk = tmp_exec_set->scheduled;
	while (is_master(tmp_prev_tsk))
	{
#ifdef QPS_ALL_BUG
		BUG_ON(master_budget_exhausted(tmp_prev_tsk));
#endif
		master_update_exec_time(tmp_prev_tsk, current_time);
#ifdef QPS_ALL_TRACE
		TRACE("in PREV master of %d: (%llu,%llu) start_exec=%llu exec=%llu exhausted=%d\n",
			master_tsk(tmp_prev_tsk).reference_cpu,
			tsk_rt(tmp_prev_tsk)->job_params.exec_time, tsk_rt(tmp_prev_tsk)->job_params.budget,
			tsk_rt(tmp_prev_tsk)->job_params.start_exec - sync_release_time, current_time - tsk_rt(tmp_prev_tsk)->job_params.start_exec, 
			master_budget_exhausted(tmp_prev_tsk) ? 1 : 0);
#endif	
		if (master_budget_exhausted(tmp_prev_tsk)) {
			tsk_rt(tmp_prev_tsk)->completed = 1;
#ifdef QPS_ALL_TRACE			
			TRACE("Master task completed\n");
#endif
		}

		switch_out_stack[s_o_n] = tmp_prev_tsk;
		s_o_n += 1;
		tmp_exec_set = client_exec_set(tmp_prev_tsk);
		tmp_prev_tsk = tmp_exec_set->scheduled;
	}

	/* This is the bottom execution set. We reschedule here since */
	/* the completion event requires to accodate the task in the release queue */
	bottom_exec_set = tmp_exec_set;
	//next_tsk = qps_reschedule2(bottom_exec_set, prev);
	
	BUG_ON((local_domain->exec_mode == S_PAR || local_domain->exec_mode == S_UNI_TO_PAR) && switch_between_exec_set);

	if (switch_between_exec_set)
	{
#ifdef QPS_ALL_TRACE
		TRACE("Switching execution set\n");
#endif
		/* do the switch */
		switch_exec_set(local_domain);		
	}

	if (local_domain->exec_mode == S_UNI) 
	{
		/* NOTE: earliest_event_time may be zero if NO_BUDGET */
		earliest_event_time = exec_set_budget_remaining(local_domain->slave_set);
		/* Check for budget overruns due to not accounted overhead (external) like the latency */
		/* NOTE: before or after */
		if (lt_after(local_domain->deadline, current_time) &&
			lt_after(earliest_event_time, (local_domain->deadline - current_time)))
		{
			/* INVARIANT: 0 < earliest_event_time < LLONG_MAX*/
#ifdef QPS_ALL_TRACE
			TRACE("WARNING: Reducing budget of %llu\n", 
				earliest_event_time - (local_domain->deadline - current_time));
#endif
			local_domain->slave_set->budget -= (earliest_event_time - (local_domain->deadline - current_time));
#ifdef QPS_ALL_BUG
			BUG_ON(lt_after(exec_set_budget_remaining(local_domain->slave_set), earliest_event_time));
#endif
			earliest_event_time = exec_set_budget_remaining(local_domain->slave_set);
		}

		exec_set_start_exec(local_domain->slave_set, current_time);
	}

	s_i_n = 0;
	tmp_exec_set = local_domain->slave_set;
	
	if (tmp_exec_set == bottom_exec_set)
		tmp_next_tsk = qps_reschedule2(tmp_exec_set, prev); //next_tsk;
	else
		tmp_next_tsk = qps_reschedule2(tmp_exec_set, tmp_exec_set->scheduled);
	
	while (is_master(tmp_next_tsk))
	{
#ifdef QPS_ALL_TRACE
		TRACE("in NEXT master of %d\n", master_tsk(tmp_next_tsk).reference_cpu);
#endif
#ifdef QPS_ALL_BUG		
		BUG_ON(master_budget_remaining(tmp_next_tsk) == LLONG_MAX);
		BUG_ON(master_budget_remaining(tmp_next_tsk) == 0LL);
#endif
		/* Check for budget overruns due to not accounted overhead (external) */
		/* NOTE: before or after */
		if (lt_after(get_deadline(tmp_next_tsk), current_time) &&
			lt_after(master_budget_remaining(tmp_next_tsk), (get_deadline(tmp_next_tsk) - current_time)))
		{
#ifdef QPS_ALL_TRACE			
			TRACE("WARNING: Reducing budget of %llu\n", 
				master_budget_remaining(tmp_next_tsk) - 
				(get_deadline(tmp_next_tsk) - current_time));
#endif
			tsk_rt(tmp_next_tsk)->job_params.budget -= 
				(master_budget_remaining(tmp_next_tsk) - 
					(get_deadline(tmp_next_tsk) - current_time));
		}

		if (master_budget_remaining(tmp_next_tsk) < earliest_event_time) {
			earliest_event_time = master_budget_remaining(tmp_next_tsk);
#ifdef QPS_ALL_TRACE
			TRACE("earliest_event_time updated %llu\n", earliest_event_time);
#endif
		}

		switch_in_stack[s_i_n] = tmp_next_tsk;
		s_i_n += 1;
		tmp_exec_set = client_exec_set(tmp_next_tsk);
		if (tmp_exec_set == bottom_exec_set)
			tmp_next_tsk = qps_reschedule2(tmp_exec_set, prev); //next_tsk;
		else
			tmp_next_tsk = qps_reschedule2(tmp_exec_set, tmp_exec_set->scheduled);
	}
#ifdef QPS_ALL_BUG
	BUG_ON(is_master(tmp_next_tsk));
#endif
	/* The next task is determined */	
	next_tsk = tmp_next_tsk;
#ifdef QPS_ALL_TRACE	
	if(next_tsk && sync_release_time)
		TRACE_TASK(next_tsk, "selected at %llu\n", litmus_clock() - sync_release_time);
#endif
	/* arm/cancel the timer */
	/* NOTE: earliest_event_time may be zero in OFF mode */
	if (lt_after(earliest_event_time, 0) && 
		lt_after(LLONG_MAX, earliest_event_time)) {
		/* This may happens */
		/*BUG_ON(lt_after(current_time + earliest_event_time, local_domain->deadline));*/
		qps_arm_enforcement_timer(local_domain, current_time + earliest_event_time);
	} else {
		qps_cancel_enforcement_timer(local_domain);
	}
	/* perform switch-in/out notifications */
	c = 0;
	while(c < qps_int_max(s_o_n, s_i_n)) {
		tmp_prev_tsk = c < s_o_n ? switch_out_stack[c] : NULL;
		tmp_next_tsk = c < s_i_n ? switch_in_stack[c] : NULL;
		qps_preempt(tmp_prev_tsk, tmp_next_tsk, local_domain->cpu, current_time);
		c += 1;
	}
#ifdef QPS_ALL_TRACE	
	TRACE("****** SCHEDULE END ******\n");
#endif
	sched_state_task_picked();
#ifdef QPS_CLUSTERED_LOCK	
	raw_spin_unlock(&dlt[local_domain->cpu]->slock);
#else
	raw_spin_unlock(&glock);
#endif

	return next_tsk;
}

/*	Prepare a task for running in RT mode. Allow to catch the barrier release 
time. */
void qps_release_at(struct task_struct *t, lt_t start)
{
	unsigned long 		flags;

	BUG_ON(!t);
#ifdef QPS_CLUSTERED_LOCK
	raw_spin_lock_irqsave(&dlt[get_partition(t)]->slock, flags);
#else
	raw_spin_lock_irqsave(&glock, flags);
#endif

	if (lt_after_eq(sync_release_time, LLONG_MAX)) {
		sync_release_time = start - t->rt_param.task_params.phase
				   + t->rt_param.task_params.period;
		TRACE("Global time initialized, system will start at %llu\n", sync_release_time);
	}

	release_at(t, start);

#ifdef QPS_CLUSTERED_LOCK	
	raw_spin_unlock_irqrestore(&dlt[get_partition(t)]->slock, flags);
#else
	raw_spin_unlock_irqrestore(&glock, flags);
#endif
	
}

/*	Prepare a task for running in RT mode
 */
static void qps_task_new(struct task_struct *t, int on_rq, int is_scheduled)
{
	qps_domain_t* 	qps_domain = task_qps_domain(t);
	exec_set_t*		exec_set;
	unsigned long	flags;
	
	TRACE_TASK(t, "qps: task new, cpu = %d, exec_set = %d\n",
		   t->rt_param.task_params.cpu, t->rt_param.task_params.set);

	/* setup job parameters */
	release_at(t, litmus_clock());
	/* The task should be running in the queue, otherwise signal
	 * code will try to wake it up with fatal consequences.
	 */
#ifdef QPS_CLUSTERED_LOCK
	raw_spin_lock_irqsave(&dlt[qps_domain->cpu]->slock, flags);
#else
	raw_spin_lock_irqsave(&glock, flags);
#endif
	
	exec_set = tsk_rt(t)->task_params.set ? 
		&qps_domain->b_set : &qps_domain->a_set;
	
	if (is_scheduled) {
		/* there shouldn't be anything else scheduled at the time */
		BUG_ON(exec_set->scheduled);
		exec_set->scheduled = t;
	} else {
		/* !is_scheduled means it is not scheduled right now, but it
		 * does not mean that it is suspended. If it is not suspended,
		 * it still needs to be requeued. If it is suspended, there is
		 * nothing that we need to do as it will be handled by the
		 * wake_up() handler. */
		if (is_running(t)) {
			requeue(t, exec_set);
			/* we probably have to reschedule */
			if (edf_preemption_needed(&exec_set->domain, exec_set->scheduled))
				preempt(exec_set->cpu);
		}
	}
#ifdef QPS_CLUSTERED_LOCK
	raw_spin_unlock_irqrestore(&dlt[qps_domain->cpu]->slock, flags);
#else
	raw_spin_unlock_irqrestore(&glock, flags);
#endif
}

static void qps_task_wake_up(struct task_struct *t)
{
	qps_domain_t* 	qps_domain = task_qps_domain(t);
	exec_set_t*		exec_set;
	lt_t			now;
	unsigned long	flags;

	TRACE_TASK(t, "wake_up at %llu\n", litmus_clock());

#ifdef QPS_CLUSTERED_LOCK
	raw_spin_lock_irqsave(&dlt[qps_domain->cpu]->slock, flags);
#else
	raw_spin_lock_irqsave(&glock, flags);
#endif

	exec_set = tsk_rt(t)->task_params.set ? 
		&qps_domain->b_set : &qps_domain->a_set;

	BUG_ON(is_queued(t));
	now = litmus_clock();
	if (is_sporadic(t) && is_tardy(t, now)) {
		/* new sporadic release */
		release_at(t, now);
		sched_trace_task_release(t);
	}

	/* Only add to ready queue if it is not the currently-scheduled
	 * task. This could be the case if a task was woken up concurrently
	 * on a remote CPU before the executing CPU got around to actually
	 * de-scheduling the task, i.e., wake_up() raced with schedule()
	 * and won.
	 */
	
	if (exec_set->scheduled != t) {
		requeue(t, exec_set);
		if (edf_preemption_needed(&exec_set->domain, exec_set->scheduled))
			preempt(exec_set->cpu);
	}

#ifdef QPS_CLUSTERED_LOCK
	raw_spin_unlock_irqrestore(&dlt[qps_domain->cpu]->slock, flags);
#else
	raw_spin_unlock_irqrestore(&glock, flags);
#endif

	TRACE_TASK(t, "wake up done\n");
}

/* TODO: when a task exit delete the master from the qps rel queue */
static void qps_task_exit(struct task_struct * t)
{
	qps_domain_t 	*qps_domain = task_qps_domain(t);
	qps_domain_t 	*tmp_domain;
	exec_set_t 		*exec_set;
	unsigned long 	flags;

#ifdef QPS_CLUSTERED_LOCK
	raw_spin_lock_irqsave(&dlt[qps_domain->cpu]->slock, flags);
#else
	raw_spin_lock_irqsave(&glock, flags);
#endif

	exec_set = get_exec_set(qps_domain, t);

	if (is_queued(t)) {
		/* dequeue */
		remove(&exec_set->domain, t);
	} 

	if (exec_set->scheduled == t)
		exec_set->scheduled = NULL;

	/* We actually remove tasks updating the fake queue at release time */
	/* We can skip here */
	if(bheap_node_in_heap(tsk_rt(t)->qps_heap_node))
		bheap_delete(qps_release_order, &qps_domain->fake_heap, tsk_rt(t)->qps_heap_node);

	/* We actually remove tasks updating the fake queue at release time */
	/* We can skip here */
	/* remove the master task if it is in the release queue */
	/*if(qps_domain->master && bheap_node_in_heap(tsk_rt(qps_domain->master)->qps_heap_node))
		bheap_delete(qps_release_order, 
			&remote_qps_domain(get_partition(qps_domain->master))->fake_heap, tsk_rt(t)->qps_heap_node);
	*/

	/* mark all processor in an exit status */
	//qps_domain->exit = 1;
	/* mark all processor in an exit status */
	tmp_domain = qps_domain;
	tmp_domain->exit = 1;
	while(tmp_domain) {
		tmp_domain->exit = 1;
		tmp_domain = tmp_domain->master ? task_qps_domain(tmp_domain->master) : NULL;
	}

	TRACE_TASK(t, "RIP, now reschedule\n");

	preempt(qps_domain->cpu);

#ifdef QPS_CLUSTERED_LOCK
	raw_spin_unlock_irqrestore(&dlt[qps_domain->cpu]->slock, flags);
#else
	raw_spin_unlock_irqrestore(&glock, flags);
#endif
}

static long qps_admit_task(struct task_struct* tsk)
{
	if (task_cpu(tsk) == tsk->rt_param.task_params.cpu) 
	{
		tsk_rt(tsk)->qps_heap_node = NULL;
		/* allocate heap node for this task */
		tsk_rt(tsk)->qps_heap_node = bheap_node_alloc(GFP_ATOMIC);

		if (!tsk_rt(tsk)->qps_heap_node) {
			printk(KERN_WARNING "litmus: no more heap node memory!?\n");
			return -ENOMEM;
		} else {
			bheap_node_init(&tsk_rt(tsk)->qps_heap_node, tsk);
		}

		tsk_rt(tsk)->task_params.master = 0;
		return 0;
	}
	else
		return -EINVAL;
}

static void update_locking_table(int from, int to) 
{
	int cpu;
	qps_domain_t *old_value = dlt[from];
	for (cpu = 0; cpu < num_online_cpus(); cpu++) {
		if (dlt[cpu] == old_value)
			dlt[cpu] = dlt[to];
		printk(KERN_INFO "dlt[%d] = %d\n", cpu, dlt[cpu]->cpu);
	}
}

static struct task_struct* new_master(void)
{
	struct task_struct *tmp_tsk = kmalloc(sizeof(struct task_struct), GFP_ATOMIC);

	tsk_rt(tmp_tsk)->heap_node = NULL;
	tsk_rt(tmp_tsk)->heap_node = bheap_node_alloc(GFP_ATOMIC);

	tsk_rt(tmp_tsk)->qps_heap_node = NULL;
	tsk_rt(tmp_tsk)->qps_heap_node = bheap_node_alloc(GFP_ATOMIC);

	if (!tmp_tsk || !tsk_rt(tmp_tsk)->heap_node ||
		 !tsk_rt(tmp_tsk)->qps_heap_node)
		BUG_ON(1);

	bheap_node_init(&tsk_rt(tmp_tsk)->heap_node, tmp_tsk);
	bheap_node_init(&tsk_rt(tmp_tsk)->qps_heap_node, tmp_tsk);

	master_tsk(tmp_tsk).master = 1;
	master_tsk(tmp_tsk).cpu = NO_CPU;
	master_tsk(tmp_tsk).executing_cpu = NO_CPU;
	master_tsk(tmp_tsk).reference_cpu = NO_CPU;
	master_tsk(tmp_tsk).set = 0;
	master_tsk(tmp_tsk).exec_cost = 0LL;
	master_tsk(tmp_tsk).period = 1LL;
	master_tsk(tmp_tsk).relative_deadline = 1LL;
	tsk_rt(tmp_tsk)->job_params.release = 0LL;
	tsk_rt(tmp_tsk)->job_params.deadline = 0LL;
	tsk_rt(tmp_tsk)->job_params.job_no = 0;
	tsk_rt(tmp_tsk)->job_params.exec_time = 0LL;
	tsk_rt(tmp_tsk)->job_params.budget = 0LL;
	tsk_rt(tmp_tsk)->job_params.start_exec = 0LL;
	tsk_rt(tmp_tsk)->job_params.stop_exec = 0LL;
	tsk_rt(tmp_tsk)->completed = 0;

	tmp_tsk->policy = SCHED_LITMUS;

	return tmp_tsk;
}

// SYSCALL (used to add a master task between two processors)
asmlinkage long sys_qps_add_master(
	int __user *__cpu, int __user *__set, int __user *__client_cpu, 
	lt_t __user *__rate_a, lt_t __user *__rate_b)
{
	long ret;
	
	int cpu, set, client_cpu;
	lt_t rate_a, rate_b;

	struct task_struct* tmp_master = NULL;

	ret = copy_from_user(&cpu, __cpu, sizeof(cpu));
	if (ret) return ret;
	ret = copy_from_user(&set, __set, sizeof(set));
	if (ret) return ret;
	ret = copy_from_user(&client_cpu, __client_cpu, sizeof(client_cpu));
	if (ret) return ret;
	ret = copy_from_user(&rate_a, __rate_a, sizeof(rate_a));
	if (ret) return ret;
	ret = copy_from_user(&rate_b, __rate_b, sizeof(rate_b));
	if (ret) return ret;

	tmp_master = new_master();

	master_tsk(tmp_master).cpu = cpu;
	master_tsk(tmp_master).set = set;
	master_tsk(tmp_master).reference_cpu = client_cpu;
	master_tsk(tmp_master).exec_cost = rate_a;
	master_tsk(tmp_master).period = rate_b;
	master_tsk(tmp_master).relative_deadline = rate_b;

	tsk_rt(tmp_master)->domain = set ? &(remote_qps_domain(cpu)->b_set.domain) : 
		&(remote_qps_domain(cpu)->a_set.domain);

	remote_qps_domain(client_cpu)->master = tmp_master;

	update_locking_table(client_cpu, cpu);
	printk(KERN_INFO "Master (%d,%d,%d,%llu,%llu) initialized!\n", cpu, set, client_cpu, rate_a, rate_b);
	return ret;
}

// SYSCALL (used to add a master task between two processors)
asmlinkage long sys_qps_add_set(
	int __user *__cpu, int __user *__set,
	lt_t __user *__rate_a, lt_t __user *__rate_b)
{
	long 			ret;
	int 			cpu, set;
	lt_t 			rate_a, rate_b;
	qps_domain_t 	*qps_domain = NULL;
	exec_set_t 		*exec_set = NULL;
	
	ret = copy_from_user(&cpu, __cpu, sizeof(cpu));
	if (ret) return ret;
	ret = copy_from_user(&set, __set, sizeof(set));
	if (ret) return ret;
	ret = copy_from_user(&rate_a, __rate_a, sizeof(rate_a));
	if (ret) return ret;
	ret = copy_from_user(&rate_b, __rate_b, sizeof(rate_b));
	if (ret) return ret;

	qps_domain = remote_qps_domain(cpu);
	exec_set = set ? &qps_domain->b_set : &qps_domain->a_set;

	exec_set->u_num = rate_a;
	exec_set->u_den = rate_b;
	
	printk(KERN_INFO "Exec set (%d,%d,%llu,%llu) initialized!\n", cpu, set, rate_a, rate_b);
	return ret;
}

static long qps_activate_plugin(void)
{
	int i;
	qps_domain_t *qps_domain;

	/* We do not really want to support cpu hotplug, do we? ;)
	 * However, if we are so crazy to do so,
	 * we cannot use num_online_cpu()
	 */
	BUG_ON(MAX_CPU < num_online_cpus());
	for (i = 0; i < num_online_cpus(); i++) {
		qps_domain = remote_qps_domain(i);
		qps_domain_init(qps_domain,
						NULL, qps_release_jobs, i);
		dlt[i] = qps_domain;
	}

	raw_spin_lock_init(&glock);
	sync_release_time = LLONG_MAX;

	return 0;
}

static long qps_deactivate_plugin(void)
{
	int i;
	qps_domain_t *qps_domain;

	/*clean up plugin status*/
	for (i = 0; i < num_online_cpus(); i++) 
	{
		qps_domain = remote_qps_domain(i);
		if (qps_domain->master) 
		{
			kfree(qps_domain->master);
			qps_domain->master = NULL;	
		}
	}
	return 0;
}

/*	Plugin object	*/
static struct sched_plugin qps_plugin __cacheline_aligned_in_smp = {

	.plugin_name		= "QPS",
	/*.tick				= qps_tick,*/
	.task_new			= qps_task_new,
	.complete_job		= complete_job,
	.task_exit			= qps_task_exit,
	.schedule			= qps_schedule,
	.task_wake_up		= qps_task_wake_up,
	.admit_task			= qps_admit_task,
	.activate_plugin	= qps_activate_plugin,
	.deactivate_plugin	= qps_deactivate_plugin,
	.release_at			= qps_release_at,

};

static int __init init_qps(void)
{
	return register_sched_plugin(&qps_plugin);
}

module_init(init_qps);
