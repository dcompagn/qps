#ifndef _QPS_H_
#define _QPS_H_

#define MAX_CPU 16
/* Budget grain must be sufficiently large to avoid */
/* multiple timer activation in short intervals */
#define QPS_MIN_BUDGET_GRAIN 40000LL

typedef enum {
	S_UNI_TO_PAR,
	S_PAR_TO_UNI,
	S_UNI,
	S_PAR,
	S_QPS_OFF,
} exec_mode_t;

typedef struct exec_set {

	int 					cpu;

	rt_domain_t 			domain;
	struct task_struct		*scheduled;

	lt_t					exec_time;
	lt_t					budget;
	lt_t					start_exec;
	lt_t					stop_exec;

	lt_t					u_num;
	lt_t					u_den;

} exec_set_t;

typedef struct qps_domain {

	int          			cpu;
	
	lt_t 					release;
	lt_t					deadline;

	int 					something_released;

	struct task_struct		*master;

	exec_set_t				a_set;
	exec_set_t 				b_set;
	exec_set_t				*master_set;
	exec_set_t				*slave_set;
	exec_mode_t				exec_mode;

	//Used to switch from sigma A to sigma B server
	struct hrtimer			timer;
	int						armed;

	struct hrtimer			dl_timer;
	int						dl_armed;

	int 					exit;
	/* to get earliest release event */
	struct bheap	 		fake_heap;

	/* scheduling lock slock */
	/* protects the domain and serializes scheduling decisions */
	#define slock a_set.domain.ready_lock

} qps_domain_t;

static inline void exec_set_start_exec(exec_set_t *exec_set, lt_t current_time)
{
	exec_set->start_exec = current_time;
}

static inline void exec_set_empty_budget(exec_set_t *exec_set)
{
	exec_set->exec_time = exec_set->budget;
}


static inline int exec_set_budget_exhausted(exec_set_t *exec_set)
{
	return lt_after_eq(exec_set->exec_time, exec_set->budget);
}

static inline lt_t exec_set_budget_remaining(exec_set_t *exec_set)
{
	if (exec_set_budget_exhausted(exec_set))
		return 0LL;
	return exec_set->budget - exec_set->exec_time;
}

static inline lt_t exec_set_budget_for_interval(exec_set_t *exec_set, lt_t start, lt_t end)
{
	return (end - start) *
		exec_set->u_num / exec_set->u_den;
}

static inline void exec_set_update_exec_time(exec_set_t *exec_set, lt_t current_time)
{
	/* INVARIANT: start_exec is set */
	/*BUG_ON(lt_after(exec_set->stop_exec, exec_set->start_exec));*/
	/*exec_set->stop_exec = current_time;*/
	/* Avoid the accumulation of overhead into exec_time over multiple time intervals */
	if (lt_after((current_time - exec_set->start_exec) + QPS_MIN_BUDGET_GRAIN, exec_set_budget_remaining(exec_set)))
		exec_set->exec_time = exec_set->budget;
	else
		exec_set->exec_time += (current_time - exec_set->start_exec);
}

static inline void master_start_exec(struct task_struct *t, lt_t current_time)
{
	tsk_rt(t)->job_params.start_exec = current_time;
}

static inline void master_empty_budget(struct task_struct *t)
{
	tsk_rt(t)->job_params.exec_time = tsk_rt(t)->job_params.budget;
}

static inline int master_budget_exhausted(struct task_struct *t)
{
	return lt_after_eq(tsk_rt(t)->job_params.exec_time, tsk_rt(t)->job_params.budget);
}

static inline lt_t master_budget_remaining(struct task_struct *t)
{
	if (master_budget_exhausted(t))
		return 0LL;
	return tsk_rt(t)->job_params.budget - tsk_rt(t)->job_params.exec_time;
}

static inline lt_t master_budget_for_interval(struct task_struct *t, lt_t start, lt_t end)
{
	return (end - start) * 
		get_exec_cost(t) / get_rt_period(t);
}

static inline void master_update_exec_time(struct task_struct *t, lt_t current_time)
{
	/* INVARIANT: start_exec is set */
	/*BUG_ON(lt_after(tsk_rt(t)->job_params.stop_exec, tsk_rt(t)->job_params.start_exec));*/
	/* Check if budget has already been exhausted */
	/*BUG_ON(master_budget_exhausted(t));*/
	/*tsk_rt(t)->job_params.stop_exec = current_time;*/
	if (lt_after((current_time - tsk_rt(t)->job_params.start_exec) + QPS_MIN_BUDGET_GRAIN, master_budget_remaining(t)))
		tsk_rt(t)->job_params.exec_time = tsk_rt(t)->job_params.budget;
	else
		tsk_rt(t)->job_params.exec_time += (current_time - tsk_rt(t)->job_params.start_exec);
	/*if (master_budget_exhausted(t))
		tsk_rt(t)->job_params.exec_time = tsk_rt(t)->job_params.budget;*/
}

static inline lt_t qps_min(lt_t a, lt_t b)
{
	return lt_after(a,b) ? b : a;
}

static inline int qps_int_max(int a, int b)
{
	return a > b ? a : b;
}

int qps_release_order(struct bheap_node* a, struct bheap_node* b)
{
	struct task_struct *first = a ? bheap2task(a) : NULL;
	struct task_struct *second = b ? bheap2task(b) : NULL;
	
	if (first && first == second)
		return 0;

	if (!first || !second)
		return first && !second;

	if (earlier_release(first, second))
		return 1;

	return 0;
}

#endif
